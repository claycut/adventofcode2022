package main

import (
	"AdventOfCode/readfile"
	"fmt"
	"strconv"
	"strings"

	"golang.org/x/exp/slices"
)

type operation int

const (
	addx operation = 1
	noop operation = 2
)

type command struct {
	op    operation
	value int
}

func getCommand(str string) command {
	op := addx
	if str == "noop" {
		op = noop
	}
	value := 0
	if op == addx {
		split := strings.Split(str, " ")
		value, _ = strconv.Atoi(split[1])
	}
	return command{op: op, value: value}
}

func completeCycle(cycle *int, xReg *int, addx int, sigStrSum *int) {
	sigStrength := *cycle * *xReg
	sumIndices := []int{20, 60, 100, 140, 180, 220}
	// fmt.Printf("During cycle %d, X = %d and the signal strength is %d\n", *cycle, *xReg, sigStrength)
	if slices.Contains(sumIndices, *cycle) {
		*sigStrSum += sigStrength
	}
	*xReg += addx
	*cycle++
}

func isPixelInSprite(pixel int, xReg int) bool {
	switch pixel {
	case xReg - 1:
		return true
	case xReg:
		return true
	case xReg + 1:
		return true
	}
	return false
}

func drawCycle(cycle *int, xReg *int, addx int) {
	pixel := (*cycle - 1) % 40

	if isPixelInSprite(pixel, *xReg) {
		fmt.Printf("█")
	} else {
		fmt.Printf(" ")
	}

	if pixel == 39 {
		fmt.Printf("\n")
	}
	*xReg += addx
	*cycle++
}

func part1(lineArray []string) {
	xReg := 1
	cycle := 1
	sigStrSum := 0
	for _, l := range lineArray {
		if l == "" {
			continue
		}
		command := getCommand(l)
		if command.op == addx {
			completeCycle(&cycle, &xReg, 0, &sigStrSum)
			completeCycle(&cycle, &xReg, command.value, &sigStrSum)
		} else {
			completeCycle(&cycle, &xReg, 0, &sigStrSum)
		}
	}
	fmt.Printf("The sum of the 6 signal strengths is %d\n", sigStrSum)
}

func part2(lineArray []string) {
	xReg := 1
	cycle := 1
	for _, l := range lineArray {
		if l == "" {
			continue
		}
		command := getCommand(l)
		if command.op == addx {
			drawCycle(&cycle, &xReg, 0)
			drawCycle(&cycle, &xReg, command.value)
		} else {
			drawCycle(&cycle, &xReg, 0)
		}
	}

}

func main() {
	lineArray := readfile.ReadTxtFile("input.txt")
	part1(lineArray)
	part2(lineArray)
	fmt.Printf("The program has ended\n")
}
