package readfile

import (
	"io/ioutil"
	"log"
	"os"
	"strings"
)

// Reads text file and returns as an array of strings where each line is a new index in the array
func ReadTxtFile(input string) []string {
	file, err := os.Open(input)
	if err != nil {
		log.Fatal(err)
	}
	lines, err := ioutil.ReadAll(file)
	if err != nil {
		log.Fatal(err)
	}
	lineArray := strings.Split(string(lines), "\n")
	return lineArray
}
