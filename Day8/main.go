package main

import (
	readfile "AdventOfCode/readFile"
	"fmt"
	"strconv"
)

type column []int

type row []column

func initializeTreeGrid(lineArray []string) row {
	rows := make(row, 0)
	for _, l := range lineArray {
		if l == "" {
			continue
		}
		c := make(column, 0)
		for _, r := range l {
			i, _ := strconv.Atoi(string(r))
			c = append(c, i)
		}
		rows = append(rows, c)
	}
	return rows
}

func isTreeVisible(x int, y int, treeGrid row) bool {
	value := treeGrid[x][y]
	left := false
	right := false
	up := false
	down := false
	for i := y - 1; i >= 0; i-- {
		if value <= treeGrid[x][i] { //test left
			left = true
			break
		}
	}
	for i := y + 1; i < len(treeGrid[x]); i++ {
		if value <= treeGrid[x][i] { //test right
			right = true
			break
		}
	}
	for n := x - 1; n >= 0; n-- {
		if value <= treeGrid[n][y] { //test up
			up = true
			break
		}
	}
	for n := x + 1; n < len(treeGrid); n++ {
		if value <= treeGrid[n][y] { //test down
			down = true
			break
		}
	}
	return !(left && right && up && down)
}

func calcScenicScore(x int, y int, treeGrid row) int {
	value := treeGrid[x][y]
	leftScore := 0
	rightScore := 0
	upScore := 0
	downScore := 0
	for i := y - 1; i >= 0; i-- {
		leftScore++
		if value <= treeGrid[x][i] { //get left
			break
		}
	}
	for i := y + 1; i < len(treeGrid[x]); i++ {
		rightScore++
		if value <= treeGrid[x][i] { //get right
			break
		}
	}
	for n := x - 1; n >= 0; n-- {
		upScore++
		if value <= treeGrid[n][y] { //get up
			break
		}
	}
	for n := x + 1; n < len(treeGrid); n++ {
		downScore++
		if value <= treeGrid[n][y] { //get down
			break
		}
	}
	return leftScore * rightScore * upScore * downScore
}

func part1(treeGrid row) {
	visibleTreeCount := 0
	for i := 1; i < len(treeGrid)-1; i++ { //skip the first and last
		col := treeGrid[i]
		for n := 1; n < len(col)-1; n++ { //skip the first and last
			// tree := col[n]
			if isTreeVisible(i, n, treeGrid) {
				visibleTreeCount++
			}
		}
	}
	//add edges to count
	visibleTreeCount += (len(treeGrid) * 2) + (len(treeGrid[0]) * 2) - 4
	fmt.Printf("The total number of visible trees is %d\n", visibleTreeCount)
}

func part2(treeGrid row) {
	//calculate scenic score
	bestScenicScore := 0
	for i := 1; i < len(treeGrid)-1; i++ { //skip the first and last
		col := treeGrid[i]
		for n := 1; n < len(col)-1; n++ { //skip the first and last
			// tree := col[n]
			score := calcScenicScore(i, n, treeGrid)
			if score > bestScenicScore {
				bestScenicScore = score
			}
		}
	}
	fmt.Printf("The best scenic score is %d\n", bestScenicScore)
}

func main() {
	lineArray := readfile.ReadTxtFile("input.txt")
	treeGrid := initializeTreeGrid(lineArray)
	part1(treeGrid)
	part2(treeGrid)
	fmt.Printf("End of program\n")
}
