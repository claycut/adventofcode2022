package main

import (
	"AdventOfCode/readfile"
	"fmt"
)

type rucksack struct {
	compartment1 compartment
	compartment2 compartment
	sharedItems  []rune
}

type compartment []rune

func isRuneInSlice(comp []rune, item rune) bool {
	for _, r := range comp {
		if item == r {
			return true
		}
	}
	return false
}

func calcPriorityScore(items []rune) int {
	score := 0
	for _, i := range items {
		asciiValue := int(i)
		if asciiValue < 91 {
			//subtract by 64
			score += (asciiValue - 38)
		} else {
			//subtract by 96
			score += (asciiValue - 96)
		}
	}
	return score
}

// compare current shared items with all shared items. Remove items that are no longer shared
func getSharedItems(allSharedItems []rune, currItems []rune) []rune {
	newList := make([]rune, 0)
	for _, aItem := range allSharedItems {
		if isRuneInSlice(currItems, aItem) {
			newList = append(newList, aItem)
		}
	}
	return newList
}

func part1(lineArray []string) []rucksack {
	allRucksacks := make([]rucksack, 0)
	totalScore := 0
	for _, l := range lineArray {
		if l == "" {
			continue
		}
		currSack := rucksack{compartment1: make(compartment, 0), compartment2: make(compartment, 0), sharedItems: make([]rune, 0)}
		compSize := len(l) / 2
		for i, r := range l {
			if i < compSize {
				currSack.compartment1 = append(currSack.compartment1, r)
			} else {
				currSack.compartment2 = append(currSack.compartment2, r)
				if isRuneInSlice(currSack.compartment1, r) && !isRuneInSlice(currSack.sharedItems, r) {
					currSack.sharedItems = append(currSack.sharedItems, r)
				}
			}
		}
		totalScore += calcPriorityScore(currSack.sharedItems)
		allRucksacks = append(allRucksacks, currSack)
	}
	fmt.Printf("The total priority score = %d\n", totalScore)
	return allRucksacks
}

func part2(allSacks []rucksack) {
	score := 0
	for i := 0; i < len(allSacks); i += 3 { //every 3rd sack
		combinedCompartment1 := append(allSacks[i].compartment1, allSacks[i].compartment2...)
		combinedCompartment2 := append(allSacks[i+1].compartment1, allSacks[i+1].compartment2...)
		combinedCompartment3 := append(allSacks[i+2].compartment1, allSacks[i+2].compartment2...)
		sharedItems := getSharedItems(combinedCompartment1, combinedCompartment2)
		sharedItems = getSharedItems(sharedItems, combinedCompartment3)[:1] //I'm not checking for duplicates, so this assumes that if there are more than one they are duplicate
		score += calcPriorityScore(sharedItems)
	}
	fmt.Printf("The priority score of the badge items is %d \n", score)
}

func main() {
	lineArray := readfile.ReadTxtFile("input.txt")
	allSacks := part1(lineArray)
	part2(allSacks)
	fmt.Printf("End of program\n")
}
