package main

import (
	"AdventOfCode/readfile"
	"fmt"
	"sort"
	"strconv"
	"strings"
)

type file struct {
	name string
	size int
}

type dir struct {
	name       string
	parent     *dir
	childFiles *[]file
	childDirs  *[]dir
}

func (d dir) getTotalSize() int {
	sum := 0
	for _, f := range *d.childFiles {
		sum += f.size
	}
	for _, d := range *d.childDirs {
		sum += d.getTotalSize()
	}
	return sum
}

func makeFileSplice() *[]file {
	files := make([]file, 0)
	return &files
}

func makeDirSplice() *[]dir {
	dirs := make([]dir, 0)
	return &dirs
}

func initFileStructure(lineArray []string) dir {
	rootDir := dir{name: "/", parent: nil, childFiles: makeFileSplice(), childDirs: makeDirSplice()}
	currentDir := &rootDir
	for _, l := range lineArray {
		if l == "" {
			continue
		}
		if strings.HasPrefix(l, "$ ") {
			//commands
			if strings.HasPrefix(l, "$ cd ") {
				if strings.HasSuffix(l, "..") {
					currentDir = currentDir.parent
				} else if !strings.HasSuffix(l, "/") {
					dirName := strings.ReplaceAll(l, "$ cd ", "")
					for _, d := range *currentDir.childDirs {
						if d.name == dirName {
							currentDir = &d
							break
						}
					}
				}
			}
		} else {
			//might need to check for duplicate files/dirs
			if strings.HasPrefix(l, "dir") {
				//create dirs
				dirName := strings.ReplaceAll(l, "dir ", "")
				*currentDir.childDirs = append(*currentDir.childDirs, dir{name: dirName, parent: currentDir, childFiles: makeFileSplice(), childDirs: makeDirSplice()})
			} else {
				//create files
				fileParts := strings.Split(l, " ")
				size, _ := strconv.Atoi(fileParts[0])
				*currentDir.childFiles = append(*currentDir.childFiles, file{name: fileParts[1], size: size})
			}
		}
	}
	fmt.Printf("Finished loading files\n")
	return rootDir
}

func getAllDirs(root dir, allDirs *[]dir) {
	for _, d := range *root.childDirs {
		*allDirs = append(*allDirs, d)
		getAllDirs(d, allDirs)
	}
}

func part1(root dir) {
	allDirs := make([]dir, 0)
	getAllDirs(root, &allDirs)
	sum := 0
	for _, d := range allDirs {
		dirSize := d.getTotalSize()
		if dirSize <= 100000 {
			sum += dirSize
		}
	}
	fmt.Printf("The sum size of directories less than 100k is %d\n", sum)
}

func part2(root dir) {
	allDirs := make([]dir, 0)
	getAllDirs(root, &allDirs)
	totalSpace := 70000000
	neededSpace := 30000000
	usedSpace := root.getTotalSize()
	freeSpace := totalSpace - usedSpace

	sizes := make([]int, 0)
	for _, d := range allDirs {
		dirSize := d.getTotalSize()
		if dirSize >= (neededSpace - freeSpace) {
			sizes = append(sizes, dirSize)
		}
	}
	sort.Ints(sizes)
	fmt.Printf("The smallest size directory that can be deleted is %d \n", sizes[0])
}

func main() {
	lineArray := readfile.ReadTxtFile("input.txt")
	root := initFileStructure(lineArray)
	part1(root)
	part2(root)
	fmt.Printf("The program has ended\n")
}
