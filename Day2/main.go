package main

import (
	"AdventOfCode/readfile"
	"fmt"
	"strings"
)

type play int

const (
	Rock     play = 1
	Paper    play = 2
	Scissors play = 3
)

func calcScore(their play, my play) int {
	//1 for rock, 2 for paper, 3 for scissor
	//0 for loss, 3 for draw, 6 for win
	switch my {
	case Rock:
		if their == Rock {
			return 1 + 3
		} else if their == Paper {
			return 1 + 0
		} else if their == Scissors {
			return 1 + 6
		}
	case Paper:
		if their == Rock {
			return 2 + 6
		} else if their == Paper {
			return 2 + 3
		} else if their == Scissors {
			return 2 + 0
		}
	case Scissors:
		if their == Rock {
			return 3 + 0
		} else if their == Paper {
			return 3 + 6
		} else if their == Scissors {
			return 3 + 3
		}
	}
	return 0
}

func setPlay(playStr string) play {
	switch playStr {
	case "A":
		return Rock
	case "X":
		return Rock
	case "B":
		return Paper
	case "Y":
		return Paper
	default:
		return Scissors
	}
}

func setPlayCorrectly(theirStr string, myStr string) (my play, their play) {
	switch theirStr {
	case "A":
		their = Rock
	case "B":
		their = Paper
	case "C":
		their = Scissors
	}

	switch myStr {
	case "X": //lose
		if their == Paper {
			my = Rock
		} else if their == Rock {
			my = Scissors
		} else if their == Scissors {
			my = Paper
		}
	case "Y": //draw
		my = their
	case "Z": //win
		if their == Paper {
			my = Scissors
		} else if their == Rock {
			my = Paper
		} else if their == Scissors {
			my = Rock
		}
	}
	return my, their
}

func part1(lineArray []string) {
	totalScore := 0
	for _, l := range lineArray {
		if l == "" {
			continue
		}
		values := strings.Split(l, " ")
		their := setPlay(values[0])
		my := setPlay(values[1])
		totalScore += calcScore(their, my)
	}
	fmt.Printf("The total score would be %d\n", totalScore)
}

func part2(lineArray []string) {
	totalScore := 0
	for _, l := range lineArray {
		if l == "" {
			continue
		}
		values := strings.Split(l, " ")
		my, their := setPlayCorrectly(values[0], values[1])
		totalScore += calcScore(their, my)
	}
	fmt.Printf("The total score would be %d\n", totalScore)
}

func main() {
	lineArray := readfile.ReadTxtFile("input.txt")
	part1(lineArray)
	part2(lineArray)
	fmt.Printf("End of program\n")
}
