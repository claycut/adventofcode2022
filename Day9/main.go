package main

import (
	readfile "AdventOfCode/readFile"
	"fmt"
	"strconv"
	"strings"
)

type position struct {
	x int
	y int
}

func moveTail(head position, tail *position) {
	xDif := head.x - tail.x
	yDif := head.y - tail.y
	if xDif > 1 {
		tail.x += 1 //move tail right 1
		if yDif > 0 {
			tail.y++
		} else if yDif < 0 {
			tail.y--
		}
	} else if xDif < -1 {
		tail.x -= 1 //move tail down 1
		if yDif > 0 {
			tail.y++
		} else if yDif < 0 {
			tail.y--
		}
	} else if yDif > 1 {
		tail.y += 1 //move tail down 1
		if xDif > 0 {
			tail.x++
		} else if xDif < 0 {
			tail.x--
		}
	} else if yDif < -1 {
		tail.y -= 1 // move tail up 1
		if xDif > 0 {
			tail.x++
		} else if xDif < 0 {
			tail.x--
		}
	}
}

func updateVisited(visited *[]position, tail position) {
	for _, pos := range *visited {
		if pos.x == tail.x && pos.y == tail.y {
			return
		}
	}
	*visited = append(*visited, tail)
}

func moveHead(command string, head *position) {
	switch command {
	case "U":
		head.y -= 1 //move head up
	case "D":
		head.y += 1 //move head down
	case "L":
		head.x -= 1 //move head left
	case "R":
		head.x += 1 //move head right
	}
}

func part1(lineArray []string) {
	head := position{x: 0, y: 0}
	tail := position{x: 0, y: 0}
	visited := make([]position, 0)
	for _, l := range lineArray {
		if l == "" {
			continue
		}
		commandParts := strings.Split(l, " ")
		distance, _ := strconv.Atoi(commandParts[1])
		for i := 0; i < distance; i++ {
			moveHead(commandParts[0], &head)
			moveTail(head, &tail)
			updateVisited(&visited, tail)
		}
	}
	fmt.Printf("The tail of the rope visited %d locations at least once\n", len(visited))
}

func part2(lineArray []string) {
	head := position{x: 0, y: 0}
	knots := make([]position, 9)
	for i := 0; i < 9; i++ {
		knots[i] = position{x: 0, y: 0}
	}

	visited := make([]position, 0)
	for _, l := range lineArray {
		if l == "" {
			continue
		}
		commandParts := strings.Split(l, " ")
		distance, _ := strconv.Atoi(commandParts[1])
		for i := 0; i < distance; i++ {
			moveHead(commandParts[0], &head)
			for n := 0; n < 9; n++ {
				if n == 0 {
					moveTail(head, &knots[n])
				} else {
					moveTail(knots[n-1], &knots[n])
				}
			}
			updateVisited(&visited, knots[8])
		}
	}
	fmt.Printf("The tail of the 9 knot rope visited %d locations at least once\n", len(visited))
}

func main() {
	lineArray := readfile.ReadTxtFile("input.txt")
	part1(lineArray)
	part2(lineArray)
	fmt.Printf("The end of the program\n")
}
