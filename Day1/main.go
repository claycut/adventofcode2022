package main

import (
	"AdventOfCode/readfile"
	"fmt"
	"sort"
	"strconv"
)

type elf struct {
	itemCalories  []int
	totalCalories int
}

func part1(lineArray []string) []elf {
	elves := make([]elf, 0)
	elves = append(elves, elf{make([]int, 0), 0})
	currentElf := &elves[len(elves)-1]
	highestCalories := 0
	for _, l := range lineArray {
		if l == "" {
			//new elf
			elves = append(elves, elf{make([]int, 0), 0})
			currentElf = &elves[len(elves)-1]
		} else {
			v, _ := strconv.Atoi(l)
			currentElf.itemCalories = append(currentElf.itemCalories, v)
			currentElf.totalCalories += v
			if currentElf.totalCalories > highestCalories {
				highestCalories = currentElf.totalCalories
			}
		}
	}
	fmt.Printf("The elf with the most calories has %d \n", highestCalories)

	return elves
}

func part2(elves []elf) {
	sort.Slice(elves, func(i, j int) bool {
		return elves[i].totalCalories > elves[j].totalCalories
	})

	fmt.Printf("The total calories for the top 3 elves = %d \n", elves[0].totalCalories+elves[1].totalCalories+elves[2].totalCalories)
}

func main() {
	lineArray := readfile.ReadTxtFile("input.txt")
	elves := part1(lineArray)
	part2(elves)
	fmt.Printf("End of program\n")
}
