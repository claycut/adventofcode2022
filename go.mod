module AdventOfCode

go 1.19

require golang.org/x/exp v0.0.0-20221208152030-732eee02a75a

require (
	github.com/RyanCarrier/dijkstra v1.1.0 // indirect
	github.com/albertorestifo/dijkstra v0.0.0-20160910063646-aba76f725f72 // indirect
	github.com/rmp4/dijkstra v1.3.0 // indirect
	seehuhn.de/go/dijkstra v0.9.0 // indirect
)
