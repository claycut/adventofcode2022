package main

import (
	"AdventOfCode/readfile"
	"fmt"
	"strconv"
	"strings"
)

type stack []rune

func (s *stack) push(r rune) {
	*s = append(*s, r)
}

func (s *stack) add(r rune) {
	*s = append([]rune{r}, *s...)
}

func (s *stack) pushMulti(r []rune) {
	*s = append(*s, r...)
}

func (s *stack) pop() rune {
	index := len(*s) - 1
	element := (*s)[index]
	*s = (*s)[:index]
	return element
}

func (s *stack) popMulti(quantity int) []rune {
	index := (len(*s) - 1) - (quantity - 1)
	elements := (*s)[index:]
	*s = (*s)[:index]
	return elements
}

func (s stack) getTopItem() rune {
	index := len(s) - 1
	return s[index]
}

func move(ship *[]stack, num int, from int, to int) {
	for i := 0; i < num; i++ {
		(*ship)[to].push((*ship)[from].pop())
	}
}

func moveMulti(ship *[]stack, quantity int, from int, to int) {
	(*ship)[to].pushMulti((*ship)[from].popMulti(quantity))
}

func addCrates(ships *[]stack, line string) {
	stackIndex := 0
	if !strings.Contains(line, "[") { //ignore header row
		return
	}
	for i := 1; i < len(line); i += 4 {
		if line[i] != ' ' {
			(*ships)[stackIndex].add(rune(line[i]))
		}
		stackIndex++
	}
}

func part1(lineArray []string) {

	ships := make([]stack, 9)
	for i := 0; i < 9; i++ {
		ships[i] = make(stack, 0)
	}

	for _, l := range lineArray {
		if len(l) == 35 {
			addCrates(&ships, l)
		}
		if strings.HasPrefix(l, "move") {
			lineSplit := strings.Split(l, " ")
			number, _ := strconv.Atoi(lineSplit[1])
			from, _ := strconv.Atoi(lineSplit[3])
			to, _ := strconv.Atoi(lineSplit[5])
			move(&ships, number, from-1, to-1)
		}
	}

	topRow := ""
	for _, s := range ships {
		topRow = topRow + string(s.getTopItem())
	}
	fmt.Printf("The top row of crates are: %q \n", topRow)
}

func part2(lineArray []string) {
	ships := make([]stack, 9)
	for i := 0; i < 9; i++ {
		ships[i] = make(stack, 0)
	}

	for _, l := range lineArray {
		if len(l) == 35 {
			addCrates(&ships, l)
		}
		if strings.HasPrefix(l, "move") {
			lineSplit := strings.Split(l, " ")
			number, _ := strconv.Atoi(lineSplit[1])
			from, _ := strconv.Atoi(lineSplit[3])
			to, _ := strconv.Atoi(lineSplit[5])
			moveMulti(&ships, number, from-1, to-1)
		}
	}

	topRow := ""
	for _, s := range ships {
		topRow = topRow + string(s.getTopItem())
	}
	fmt.Printf("The top row of crates are: %q \n", topRow)
}

func main() {
	lineArray := readfile.ReadTxtFile("input.txt")
	part1(lineArray)
	part2(lineArray)
	fmt.Printf("End of program\n")
}
