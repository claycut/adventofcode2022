package main

import (
	readfile "AdventOfCode/readFile"
	"fmt"
	"sort"
	"strconv"
	"strings"
)

type operationType int

const (
	multiplication operationType = iota
	substract
	addition
	division
)

type operation struct {
	operType operationType
	val1     int
	val2     int
}

type monkey struct {
	items           []int
	op              operation
	testValue       int
	trueMonkey      int
	falseMonkey     int
	inspectionCount int
}

func (o operation) performOperation(old int) int {
	val2 := old
	if o.val2 != 0 {
		val2 = o.val2
	}
	switch o.operType {
	case multiplication:
		return old * val2
	case substract:
		return old - val2
	case addition:
		return old + val2
	default:
		return old / val2
	}
}

func loadMonkeys(lineArray []string) []monkey {
	monkeyList := make([]monkey, 0)

	currMonkey := monkey{inspectionCount: 0}
	for _, l := range lineArray {
		l = strings.Trim(l, " ")
		if l == "" {
			//complete monkey
			monkeyList = append(monkeyList, currMonkey)
			currMonkey = monkey{inspectionCount: 0}
		}
		if strings.HasPrefix(l, "Starting items: ") {
			l = strings.ReplaceAll(l, "Starting items: ", "")
			split := strings.Split(l, ", ")
			for _, item := range split {
				i, _ := strconv.Atoi(item)
				currMonkey.items = append(currMonkey.items, i)
			}
		} else if strings.HasPrefix(l, "Operation: new = ") {
			l = strings.ReplaceAll(l, "Operation: new = ", "")
			split := strings.Split(l, " ")
			op := operation{}
			switch split[0] {
			case "old":
				op.val1 = 0
			default:
				val1, _ := strconv.Atoi(split[0])
				op.val1 = val1
			}
			switch split[1] {
			case "*":
				op.operType = multiplication
			case "-":
				op.operType = substract
			case "+":
				op.operType = addition
			case "/":
				op.operType = division
			}
			switch split[2] {
			case "old":
				op.val1 = 0
			default:
				val2, _ := strconv.Atoi(split[2])
				op.val2 = val2
			}
			currMonkey.op = op
		} else if strings.HasPrefix(l, "Test: divisible by ") {
			l = strings.ReplaceAll(l, "Test: divisible by ", "")
			testVal, _ := strconv.Atoi(l)
			currMonkey.testValue = testVal
		} else if strings.HasPrefix(l, "If true: throw to monkey ") {
			l = strings.ReplaceAll(l, "If true: throw to monkey ", "")
			monIndex, _ := strconv.Atoi(l)
			currMonkey.trueMonkey = monIndex
		} else if strings.HasPrefix(l, "If false: throw to monkey ") {
			l = strings.ReplaceAll(l, "If false: throw to monkey ", "")
			monIndex, _ := strconv.Atoi(l)
			currMonkey.falseMonkey = monIndex
		}
	}

	return monkeyList
}

func part1(monkeyList []monkey) {
	for n := 0; n < 20; n++ {
		for i, monkey := range monkeyList {
			for _, item := range monkey.items {
				monkeyList[i].inspectionCount++
				new := monkey.op.performOperation(item)
				new /= 3
				if new%monkey.testValue == 0 {
					//throw to monkey...
					monkeyList[monkey.trueMonkey].items = append(monkeyList[monkey.trueMonkey].items, new)
				} else {
					//throw to monkey..
					monkeyList[monkey.falseMonkey].items = append(monkeyList[monkey.falseMonkey].items, new)
				}
			}
			monkeyList[i].items = make([]int, 0)
		}
	}

	for i, m := range monkeyList {
		fmt.Printf("Monkey %d has an inspection count value of %d after 20 rounds\n", i, m.inspectionCount)
	}

	sort.Slice(monkeyList, func(i, j int) bool {
		return monkeyList[i].inspectionCount > monkeyList[j].inspectionCount
	})

	fmt.Printf("The top two active monkeys have counts of %d and %d for a level of monkey business of %d\n", monkeyList[0].inspectionCount, monkeyList[1].inspectionCount, monkeyList[0].inspectionCount*monkeyList[1].inspectionCount)

}

func part2(monkeyList []monkey) {

	mod := 1
	for _, monkey := range monkeyList {
		mod *= monkey.testValue
	}
	for n := 0; n < 10000; n++ {
		for i, monkey := range monkeyList {
			for _, item := range monkey.items {
				monkeyList[i].inspectionCount++
				new := monkey.op.performOperation(item)
				new = new % mod
				if new%monkey.testValue == 0 {
					//throw to monkey...
					monkeyList[monkey.trueMonkey].items = append(monkeyList[monkey.trueMonkey].items, new)
				} else {
					//throw to monkey..
					monkeyList[monkey.falseMonkey].items = append(monkeyList[monkey.falseMonkey].items, new)
				}
			}
			monkeyList[i].items = make([]int, 0)
		}
	}

	for i, m := range monkeyList {
		fmt.Printf("Monkey %d has an inspection count value of %d after 10000 rounds\n", i, m.inspectionCount)
	}

	sort.Slice(monkeyList, func(i, j int) bool {
		return monkeyList[i].inspectionCount > monkeyList[j].inspectionCount
	})

	fmt.Printf("The top two active monkeys have counts of %d and %d for a level of monkey business of %d\n", monkeyList[0].inspectionCount, monkeyList[1].inspectionCount, monkeyList[0].inspectionCount*monkeyList[1].inspectionCount)

}

func main() {
	lineArray := readfile.ReadTxtFile("input.txt")
	monkeyList := loadMonkeys(lineArray)
	part1(monkeyList)
	monkeyList = loadMonkeys(lineArray)
	part2(monkeyList)
	fmt.Printf("The end of the program\n")
}
