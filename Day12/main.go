package main

import (
	"AdventOfCode/readfile"
	"fmt"
	"strconv"

	// "github.com/albertorestifo/dijkstra"
	"github.com/rmp4/dijkstra"
)

func createGrid(lineArray []string) [][]rune {
	rows := make([][]rune, 0)
	for _, l := range lineArray {
		if l == "" {
			continue
		}
		col := make([]rune, 0)
		for _, c := range l {
			col = append(col, c)
		}
		rows = append(rows, col)
	}
	return rows
}

func getNodeValue(r rune) int {
	switch r {
	case 'S':
		return 0
	case 'E':
		return 27
	default:
		return int(r) - 96
	}
}

func part1(rows [][]rune) {

	graph := dijkstra.NewGraph()
	startNode, endNode := 0, 0
	count := 0
	for _, r := range rows {
		for range r {
			nodeName, _ := strconv.Atoi(fmt.Sprint(count))
			graph.AddVertex(nodeName)
			count++
		}
	}
	count = 0
	for y, r := range rows {
		for x, c := range r {
			nodeName, _ := strconv.Atoi(fmt.Sprint(count))

			v := getNodeValue(c)
			if v == 0 {
				startNode = nodeName
			} else if v == 27 {
				endNode = nodeName
			}
			//check top
			if y > 0 && getNodeValue(rows[y-1][x])-v <= 1 {
				topNodeName, _ := strconv.Atoi(fmt.Sprint(count - len(rows[y])))
				graph.AddArc(nodeName, topNodeName, 1)
			}
			//check bottom
			if y < len(rows)-1 && getNodeValue(rows[y+1][x])-v <= 1 {
				bottomNodeName, _ := strconv.Atoi(fmt.Sprint(count + len(rows[y])))
				graph.AddArc(nodeName, bottomNodeName, 1)
			}
			//check left
			if x > 0 && getNodeValue(rows[y][x-1])-v <= 1 {
				leftNodeName, _ := strconv.Atoi(fmt.Sprint(count - 1))
				graph.AddArc(nodeName, leftNodeName, 1)
			}
			//check right
			if x < len(rows[y])-1 && getNodeValue(rows[y][x+1])-v <= 1 {
				rightNodeName, _ := strconv.Atoi(fmt.Sprint(count + 1))
				graph.AddArc(nodeName, rightNodeName, 1)
			}
			count++
		}
	}

	best, error := graph.Shortest(startNode, endNode)

	if error == nil {
		// for _, b := range best {
		fmt.Printf("The total cost for shortest path is %d\n", best.Distance)
		// }
	}

}

func part2(lineArray []string) {

}

func main() {
	lineArray := readfile.ReadTxtFile("input.txt")
	rows := createGrid(lineArray)
	part1(rows)

	fmt.Printf("End of program\n")
}
