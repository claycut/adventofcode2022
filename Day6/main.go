package main

import (
	"AdventOfCode/readfile"
	"fmt"
)

func isUniqueSet(set []rune) bool {
	for i := 0; i < len(set); i++ {
		for n := len(set) - 1; n > i; n-- {
			if set[i] == set[n] {
				return false
			}
		}
	}
	return true
}

func part1(datastream []rune) {

	for i := 0; i < len(datastream)-4; i++ {
		if isUniqueSet(datastream[i : i+4]) {
			fmt.Printf("The first start of packet marker is at index %d \n", i+4)
			break
		}
	}

}

func part2(datastream []rune) {
	for i := 0; i < len(datastream)-14; i++ {
		if isUniqueSet(datastream[i : i+14]) {
			fmt.Printf("The first start of message marker is at index %d \n", i+14)
			break
		}
	}
}

func main() {
	lineArray := readfile.ReadTxtFile("input.txt")
	part1([]rune(lineArray[0]))
	part2([]rune(lineArray[0]))
	fmt.Printf("End of program\n")
}
