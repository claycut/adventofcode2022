package main

import (
	"AdventOfCode/readfile"
	"fmt"
	"strconv"
	"strings"
)

type sectionRange struct {
	min int
	max int
}

func createSectionRange(minString string, maxString string) sectionRange {
	min, _ := strconv.Atoi(minString)
	max, _ := strconv.Atoi(maxString)
	return sectionRange{min: min, max: max}
}

func isPairFullyContained(assign1 sectionRange, assign2 sectionRange) bool {
	if assign1.min >= assign2.min && assign1.max <= assign2.max { //assign1 is contained inside assign2
		return true
	}
	if assign2.min >= assign1.min && assign2.max <= assign1.max { //assign1 is contained inside assign2
		return true
	}
	return false
}

func isOverlapPair(assign1 sectionRange, assign2 sectionRange) bool {
	if (assign1.min >= assign2.min && assign1.min <= assign2.max) || (assign1.max >= assign2.min && assign1.max <= assign2.max) {
		return true
	}
	if (assign2.min >= assign1.min && assign2.min <= assign1.max) || (assign2.max >= assign1.min && assign2.max <= assign1.max) {
		return true
	}
	return false
}

func part1(lineArray []string) {
	fullyContainedCount := 0
	overlapCount := 0
	for _, l := range lineArray {
		if l == "" {
			continue
		}

		assignPairs := strings.Split(l, ",")
		assign1Split := strings.Split(assignPairs[0], "-")
		assign1 := createSectionRange(assign1Split[0], assign1Split[1])
		assign2Split := strings.Split(assignPairs[1], "-")
		assign2 := createSectionRange(assign2Split[0], assign2Split[1])

		if isPairFullyContained(assign1, assign2) {
			fullyContainedCount++
		}
		if isOverlapPair(assign1, assign2) {
			overlapCount++
		}
	}
	fmt.Printf("The number of assignment pairs that fully contain the other is %d \n", fullyContainedCount)
	fmt.Printf("The number of assignment pairs that overlap the other is %d \n", overlapCount)
}

func main() {
	lineArray := readfile.ReadTxtFile("input.txt")
	part1(lineArray) //this is part 1 and 2
	fmt.Printf("End of program\n")
}
